#ifndef __el_timeleft_h
#define __el_timeleft_h

#include "../release.h"
#include "../coordinate.h"

#include <iostream>


namespace PRJID {


// a countdown
class Timeleft {
	C3f scale;
	C3f trans;
	C3 color;

	unsigned int max;
	unsigned int current;
public:
	Timeleft(unsigned int m=1000);
	~Timeleft();
	

	unsigned int get_current() { return current; }
	unsigned int get_max() { return max; }
	unsigned int get_left() { if (current>=max) return 0; return max-current; }
	bool over();

	void reset();

	bool tick(unsigned int ticks);

	int percent();

	void dim(C3 c,C3f s,C3f t);
	void draw();
};


} //namespace

#endif
