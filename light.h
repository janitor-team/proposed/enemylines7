#ifndef __el__light_h
#define __el__light_h

#include <iostream>
#include "release.h"

namespace PRJID {


class Light {

public:

	static void light(int codepos);
	static void fog();
	static void llight0(int codepos);
	static void llight1(int codepos);
	static void llight2(int codepos);

	static void start_frame();
};

std::ostream& operator<<(std::ostream&s, Light);

} //namespace

#endif
