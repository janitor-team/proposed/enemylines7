

#include "blockinfo.h"
#include "block.h"

namespace block {


Blockinfo3::Blockinfo3() {
	scale_=1;
	level_=0;
	block_=NULL;
	pos_=C3f();
}


Blockinfo3 Blockinfo3::get_descend(unsigned int i) const{
	Blockinfo3 n=*this;
	n.descend(i);
	return n;
}

void Blockinfo3::descend(unsigned int i) {
	if (i>7) {
		return;
	}

	level_++;
	scale_*=0.5;
	pos_+=Block::sub_positionf(i)*scale_;
	block_=block_->get_child(i);
}


} //namespace
