#ifndef __block__cacher_h
#define __block__cacher_h

#include <iostream>
#include <map>

#include "coordinate.h"

#include "blockinfo.h"

namespace block {

class Block;

class Cacher {
	std::map<std::string,unsigned char> corners;
	unsigned int stat;
	unsigned int stat_drawn;
	unsigned int level;
	bool should_draw(C3f p,int side,int level);
	bool count_corner(C3f p,int side,int level);
public:
	Cacher();
	void cache(Block *b,unsigned int level);
	void paint(Blockinfo3 b,bool draw);
	void count(Blockinfo3 b);
};


} //namespace

#endif
