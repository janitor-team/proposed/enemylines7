

#include "infostack.h"

namespace block {

Blockinfo3 Infostack::top() {
	if (empty()) return Blockinfo3();
	return infos[infos.size()-1];

}

bool Infostack::empty() {
	return (infos.size()==0);
}
void Infostack::push(Blockinfo3 b) {
	infos.push_back(b);
}
void Infostack::pop() {
	if (empty()) return;
	infos.pop_back();
}
Block *Infostack::get_current() {
	if (empty()) return NULL;

	return top().get_current();
}

std::ostream& operator<<(std::ostream&s, Infostack sk) {
	return s;
}

} //namespace
