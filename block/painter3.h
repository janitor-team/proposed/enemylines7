#ifndef __block__painter3_h
#define __block__painter3_h

#include <iostream>

#include "coordinate.h"

#include "blockinfo.h"


// usable editmode painter  with culling

namespace block {

class Block;


class Painter3 {

public:

	static void paint(Block *b, Block *selected=NULL,bool showall=false);
	static void paint(Blockinfo3 b,Block *selected=NULL,bool showall=false);
};


} //namespace

#endif
