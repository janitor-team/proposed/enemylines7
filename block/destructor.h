#ifndef __block__destructor_h
#define __block__destructor_h

#include <iostream>

#include "coordinate.h"

#include "blockinfo.h"

namespace block {

class Block;

class Destructor {

	unsigned int destroyed;
public:
	bool destruct(Block *b,C3f p,float s);
	bool destruct(Blockinfo3 b,C3f p,float s);
	
	unsigned int result() { return destroyed; }
};


} //namespace

#endif
