#ifndef __block__material_h
#define __block__material_h

#include <iostream>

#include "coordinate.h"

namespace block {


typedef enum E_Material {
	M_NONE,
	M_SAVE_SPLITTED,
	M_DEFAULT,

	M_WALL1,
	M_WALL2,
	M_WALL3,
	M_WALL4,
	M_WALL5,
	M_WALL6,
	M_WINDOW1,
	M_WINDOW2,
	M_WINDOW3,
	M_WINDOW4,
	M_STREET1,
	
	M_BOMBEDWALL1,
	M_BOMBEDWALL2,
	M_BOMBEDWALL3,

	M_SLOPE_BOMBED1_1,
	M_SLOPE_BOMBED1_2,
	M_SLOPE_BOMBED2_1,
	M_SLOPE_BOMBED2_2,
	M_SLOPE_BOMBED3_1,
	M_SLOPE_BOMBED3_2,
	M_SLOPE_BOMBED4_1,
	M_SLOPE_BOMBED4_2,
	
	M_LASTNORMAL,

//for drawing only
	M_NONEEDIT,
	M_NONESELECTED,
	M_SELECTED,

	M_LAST
};


void draw_material(E_Material m,int side=-1);
void draw_select(C3f pos,float scale);


} //namespace

#endif
