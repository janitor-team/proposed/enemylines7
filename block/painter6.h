#ifndef __block__painter6_h
#define __block__painter6_h

#include <iostream>

#include "coordinate.h"

#include "blockinfo.h"


// usable release painter  with culling and displaylist calling

namespace block {

class Block;


class Painter6 {

public:

	static void paint(Block *b);
	static void paint(Blockinfo3 b);
};


} //namespace

#endif
