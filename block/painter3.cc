

#include "painter3.h"

#include "material.h"
#include "block.h"
#include "tweak/tweak.h"
#include "cube.h"

namespace block {

	bool frustum_test(C3f testpos,float scale) {
		testpos*=Tweak::blockscale_f();
		if (Frustum::instance()->test_point(testpos)) { return true; }
		scale*=Tweak::blockscale_f();
		if (Frustum::instance()->test_corners(testpos,scale,scale)) return true;
		return false;	
	}

void Painter3::paint(Block *b,Block *selected,bool showall) {
	Blockinfo3 bi;
	bi.block_=b;
	paint(bi,selected,showall);
}
void Painter3::paint(Blockinfo3 bi,Block *selected,bool showall) {
	if (bi.block_->is_splitted()) {
		if (bi.level_>3&&!frustum_test(bi.pos_,bi.scale_)) {
			Frustum::instance()->notdrawn();
			return;
		}
		if (bi.block_==selected) {
			draw_select(bi.pos_,bi.scale_);
		}
		for (unsigned int i=0;i<8;i++) {
			paint(bi.get_descend(i),selected,showall);
		}
		return;
	}
	E_Material mat;

	mat=bi.block_->get_material();
	if (mat==M_NONE) {
		if (!showall) return;
		mat=M_NONEEDIT;
	}
	if (bi.block_==selected) {
		draw_select(bi.pos_,bi.scale_);
	}
	if (bi.level_<8&&!frustum_test(bi.pos_,bi.scale_)) {
		Frustum::instance()->notdrawn();
		return;
	}
	Frustum::instance()->drawn();

	glPushMatrix();
	glTranslatef(bi.pos_.x,bi.pos_.y,bi.pos_.z);	
	glScalef(bi.scale_,bi.scale_,bi.scale_);
	draw_material(mat);
	glPopMatrix();
}


} //namespace
