#ifndef __block__debugger_h
#define __block__debugger_h

#include <iostream>

#include "coordinate.h"

#include "blockinfo.h"

namespace block {

class Block;


class Debugger {

public:

	static void debug(Block *b, Block *selected=NULL,bool showall=false);
	static void debug(Blockinfo3 b,Block *selected=NULL,bool showall=false);
};


} //namespace

#endif
