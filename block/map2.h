#ifndef __el__map2_h
#define __el__map2_h

#include <iostream>

#include "coordinate.h"
#include "block.h"

#include <string>

#include "SDL_opengl.h"

#include "infostack.h"
namespace block {


class Map2 {
	std::string copystring;
	Block block;
	Infostack selected;
public:

	void init(std::string name="");
	void draw();

	void check();

	Block *get_block() { return &block; }


	void select(unsigned int x,unsigned int y,int level=-1);

	void load(std::string name="");
	void save();
	void copy();
	void paste();

	bool does_collide(C3f p,float size);

	void cycle_material();
	void set_material();
	void toggle_selected();
	void split_selected();
	void merge_selected();
	void select_parent();

	unsigned int destroy(C3f p,float radius);

	void debug();

	static void set_path(std::string p);
};


} //namespace

#endif
