#include "SDL_opengl.h"

#include "font_ogl.h"
#include "coordinate.h"

#include "release.h"

#include "help.h"

#include "config.h"

#include "tweak/tweak.h"

namespace PRJID {





void Help::gameplay() {
   static GLuint dl=0;
   if (dl!=0) { glCallList(dl); return; }
   dl = glGenLists(1);
   glNewList(dl,GL_COMPILE);
	glColor3ub(250,250,250);
	glPushMatrix();
	glTranslatef(10,350,0);

	Font_ogl::write("Enemy bombers are approaching Glysen, our capital.");
	glTranslatef(0,14,0);
	Font_ogl::write("The situation is without hope.");
	glTranslatef(0,14,0);
	Font_ogl::write("But with the new experimental mech we");
	glTranslatef(0,14,0);
	Font_ogl::write("developed you can at least try to");
	glTranslatef(0,14,0);
	Font_ogl::write("take as many of them with you as possible.");
	glTranslatef(0,14,0);
	Font_ogl::write("Good hunting.");
	glTranslatef(0,14,0);
	Font_ogl::write("The green bar shows shelter structural integrity.");
	glTranslatef(0,14,0);
	Font_ogl::write("");
	glPopMatrix();
	glEndList();
}

void Help::mouselock() {
	glColor3f(1,1,0);
	Font_ogl::write(C3(320,240+60),"Click to grab mouse!",false,FA_CENTER);
}

void Help::mouselock_off() {
	glColor3f(1,1,0);
	Font_ogl::write(C3(320,240+60),"Mouse grabbed.",false,FA_CENTER);
	Font_ogl::write(C3(320,240+80),"Press 'm' if you wish to release it.",false,FA_CENTER);
}

void Help::controls() {
   static GLuint dl=0;
   if (dl!=0) { glCallList(dl); return; }
   dl = glGenLists(1);
   glNewList(dl,GL_COMPILE);
	glColor3ub(250,250,250);
	glPushMatrix();
	glTranslatef(330,350,0);

	Font_ogl::write("'m' to toggle mouselook! 'r' to invert mouse");
	glTranslatef(0,14,0);
	Font_ogl::write("w,a,s,d or arrow keys to move.");
	glTranslatef(0,14,0);
	Font_ogl::write("Left Mousebutton fires gun.");
	glTranslatef(0,14,0);
	Font_ogl::write("Space activates Jumpjets.");
	glTranslatef(0,14,0);
	Font_ogl::write("Escape to exit.");
	glTranslatef(0,14,0);
	Font_ogl::write("f to toggle fullscreen.");
	glTranslatef(0,14,0);
	Font_ogl::write("v to toggle audio.");
	glTranslatef(0,14,0);
	Font_ogl::write("F1 or h for this help.");
	glPopMatrix();
	glEndList();
}

void Help::paused() {
	glColor3ub(250,250,250);
	glPushMatrix();
	glScalef(2,2,2);
	glTranslatef(125,10,0);
	Font_ogl::write("game paused");
	glPopMatrix();
}

void Help::title() {
	glColor3ub(250,250,250);
	glPushMatrix();
	glScalef(2,2,2);
	glTranslatef(125,22,0);
	Font_ogl::write(FULLNAME);
	glPopMatrix();
}

void Help::gameover() {
   static GLuint dl=0;
   if (dl!=0) { glCallList(dl); return; }
   dl = glGenLists(1);
   glNewList(dl,GL_COMPILE);
	glColor3ub(250,250,250);
	glPushMatrix();
	glTranslatef(
		Tweak::ui_gameover_x_i(),
		Tweak::ui_gameover_y_i(),0);
	Font_ogl::write("        Game over       ",false,FA_CENTER);
	glTranslatef(0,14,0);
	Font_ogl::write("Press Escape to exit",false,FA_CENTER);
	glPopMatrix();
	glEndList();
}

void Help::fin() {
   static GLuint dl=0;
   if (dl!=0) { glCallList(dl); return; }
   dl = glGenLists(1);
   glNewList(dl,GL_COMPILE);
	glColor3ub(250,250,250);
	glPushMatrix();
	glTranslatef(
		Tweak::ui_gameover_x_i(),
		Tweak::ui_gameover_y_i(),0);
	Font_ogl::write("        You made it!       ",false,FA_CENTER);
	glTranslatef(0,14,0);
	Font_ogl::write("Press Escape to exit",false,FA_CENTER);
	glPopMatrix();
	glEndList();
}

} //namespace
