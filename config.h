#ifndef __el__config_h
#define __el__config_h

#include <iostream>

#include "release.h"

namespace PRJID {


class Config {

public:

	static int mouse_reverse();
	static void toggle_mouse_reverse();


	static void resolution(unsigned int x,unsigned int y);

	static unsigned int dx();
	static unsigned int dy();

   static unsigned int cx();
   static unsigned int cy();

   static unsigned int tx();
   static unsigned int ty();



	static bool record();
	static void toggle_record();

	static bool show_gun();
	static void toggle_show_gun();

};


} //namespace

#endif
