#ifndef __el__help_h
#define __el__help_h

#include <iostream>

#include "game.h"

#include "release.h"

namespace PRJID {


class Help {

public:

static void controls();
static void gameplay();

static void hud();
static void paused();
static void title();

static void fin();

static void gameover();
static void mouselock();
static void mouselock_off();

};


} //namespace

#endif
