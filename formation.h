#ifndef __el__formation_h
#define __el__formation_h

#include <iostream>
#include "release.h"

namespace PRJID {


class Entity;

namespace Formation {

	void apply(Entity *e);
	void apply2(Entity *e);

}


} //namespace

#endif
