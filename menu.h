#ifndef __el__menu_h
#define __el__menu_h

#include <iostream>
#include <vector>

#include "coordinate.h"

class Menuitem;

typedef void (*menufun)(Menuitem *);


typedef enum E_Menuid {
	MI_LAST=0,
	MI_UNHANDLED=-1,
	MI_SUB=-2,
	MI_POP=-3,
	MI_INACTIVE=-4,
	MI_NONE=-5
};

class Menuitem {
public:
	std::vector <Menuitem> items;
	Menuitem *parent;
	std::string text;
	int id;
	menufun fun;

	void init() {
		fun=NULL;
		parent=NULL;
		id=MI_NONE;
		text="";
	}

	Menuitem() {init();};
	Menuitem(std::string s,int i) {init(); text=s; id=i;}
	Menuitem(menufun f,int i) {init(); fun=f; id=i;}
};

class Menu {
	Menuitem baseitem;
	Menuitem *current;
	Menuitem *addcurrent;
	int selected;
	void handle(int id);
	int scale;
	C3 offset;
	bool smooth;
public:

	Menu();
	Menu(int s,C3 os,bool sm=true);

	bool isactive() { return current!=NULL; }

	void reset();

	

	void add(Menuitem i);
	void sub();
	void parent();

	void select(int x,int y);

	void select(int s);
	void move_select(int c);

	void draw();
	int get(); 
	Menuitem *get_item();
	int click(int x,int y);

	void pop();

	int handle_event(SDL_Event event);

};

#endif
