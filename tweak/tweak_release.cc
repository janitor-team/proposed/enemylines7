#include "tweak_release.h"
namespace Tweak {
void tick() {}
int   balance_extrawave1_i () { return 110000; }
int   balance_extrawave2_i () { return 300000; }
int   balance_extrawave_random_i () { return 20000; }
int   balance_inbetweenwave_i () { return 200000; }
int   balance_inbetweenwave_random_i () { return 20000; }
int   balance_jumpjets_energy_i () { return 250; }
int   balance_jumpjets_regen_i () { return 100; }
int   balance_max_destruction_i () { return 10000; }
int   balance_regen_interval_i () { return 50000; }
int   balance_wave_interval_max_i () { return 17000; }
int   balance_wave_interval_min_i () { return 2000; }
int   balance_weapon_energy_i () { return 300; }
int   balance_weapon_regen_i () { return 150; }
int   balance_win_time_i () { return 900000; }
float blockscale_f () { return 80; }
float bomb_bombedmat_fact_f () { return 0.7; }
float bomb_boxsphere_fact_f () { return 0.9; }
float bomb_radius_f () { return 2.9; }
float bomb_radius_random_f () { return 1; }
int   bomb_splitlevel_i () { return 6; }
float bomb_ycheat_f () { return 0; }
int   coloredcubes_i () { return 0; }
C3    cube_back_color () { return C3(254,0,0); }
C3    cube_bottom_color () { return C3(0,172,0); }
C3    cube_front_color () { return C3(0,198,145); }
C3    cube_left_color () { return C3(222,89,0); }
C3    cube_right_color () { return C3(0,84,126); }
C3    cube_top_color () { return C3(255,255,0); }
int   debug_addenemies_i () { return 1; }
int   debug_collisionoff_i () { return 0; }
int   debug_flymode_i () { return 0; }
int   debug_hidemap_i () { return 0; }
float debug_planerot_f () { return 180; }
float debug_rotx_f () { return 0; }
int   debug_select_level_i () { return 5; }
int   debugdraw_i () { return 0; }
int   draw_all_i () { return 0; }
int   editmode_i () { return 0; }
C3f    floor_biosphere_pos_C3f () { return C3f(-70,-0.1,-75); }
C3f    floor_bunker_pos_C3f () { return C3f(-100,-0.5,50); }
C3f    floor_station_pos_C3f () { return C3f(105,-0.5,-90); }
C3f    floor_tower_pos_C3f () { return C3f(120,-0.5,120); }
C3    fog_color () { return C3(1,38,43); }
float fog_density_f () { return 1.6; }
float fog_end_f () { return 150; }
float fog_start_f () { return 50; }
float formation_dist_f () { return 3; }
float formation_removefact_f () { return 1.1; }
int   formation_spawn_i () { return 155; }
float inactive_scale_f () { return 0.1; }
float inactive_trans_f () { return 0.5; }
C3    laser_color () { return C3(0,255,83); }
int   light0_codepos_i () { return 1; }
C3    light0_color () { return C3(255,249,255); }
C3f    light0_position_C3f () { return C3f(30,30,19); }
int   light1_codepos_i () { return 1; }
C3    light1_color () { return C3(132,140,131); }
C3f    light1_position_C3f () { return C3f(-1,-1,-1); }
int   light2_codepos_i () { return -1; }
C3    light2_color () { return C3(83,83,86); }
C3f    light2_position_C3f () { return C3f(); }
int   map_size_i () { return 40; }
C3    material_bombedwall1_color () { return C3(38,40,48); }
int   material_current_i () { return 13; }
C3    material_slope_bombed1_color () { return C3(38,40,48); }
C3    material_street1_color () { return C3(91,94,99); }
C3    material_wall1_color () { return C3(228,233,217); }
C3    material_wall2_color () { return C3(255,255,255); }
C3    material_wall3_color () { return C3(214,233,230); }
C3    material_wall4_color () { return C3(188,180,192); }
C3    material_window1_color () { return C3(255,255,0); }
C3    material_window2_color () { return C3(254,204,0); }
float phys_bombfallconst_f () { return 0.5; }
float phys_fallconst_f () { return 0.2; }
int   plane_1_bombrandom_i () { return 500; }
float plane_1_speed_f () { return 0.4; }
int   plane_1_ttl_i () { return 50000; }
int   plane_2_bombrandom_i () { return 400; }
float plane_2_speed_f () { return 0.3; }
int   plane_2_ttl_i () { return 50000; }
int   plane_score_i () { return 10; }
int   plane_waveinter_diffi_i () { return 1000; }
int   plane_waveinter_i () { return 18000; }
float plane_y_base_f () { return 20; }
float plane_y_random_f () { return 20; }
float player_height_f () { return 0.4; }
int   player_jump_i () { return 300; }
float player_max_pos_f () { return 130; }
float player_min_pos_f () { return -50; }
float player_speed_f () { return 0.2; }
int   playmode_i () { return 1; }
float projectile_forward_f () { return 4; }
float projectile_speed_f () { return 3; }
float projectile_starty_f () { return 0; }
int   projectile_ttl_i () { return 2400; }
int   radio_duration_default_i () { return 7000; }
C3f    radio_pos_C3f () { return C3f(0,340,0); }
C3f    radio_scale_C3f () { return C3f(160,140,0); }
int   radio_timer_start_i () { return 5000; }
float rot_max_x_f () { return 80; }
float rot_min_x_f () { return -80; }
C3    skybox1_color () { return C3(0,32,30); }
C3    skybox2_color () { return C3(0,28,17); }
C3    skybox3_color () { return C3(0,19,22); }
C3    skybox4_color () { return C3(0,20,19); }
C3    skybox5_color () { return C3(0,20,17); }
C3    skybox6_color () { return C3(0,16,17); }
C3    skybox7_color () { return C3(73,20,25); }
C3    skybox8_color () { return C3(0,19,20); }
int   skybox_draw_i () { return 1; }
float skybox_size_f () { return 149; }
C3f    tower_pos_C3f () { return C3f(-65,-0.5,-38); }
C3    ui_destruction_color () { return C3(0,228,0); }
int   ui_gameover_x_i () { return 320; }
int   ui_gameover_y_i () { return 200; }
C3    ui_jumpjets_color () { return C3(255,0,249); }
int   ui_mouselock_i () { return 12000; }
int   ui_score_x_i () { return 11; }
int   ui_score_y_i () { return 12; }
C3    ui_weapon_color () { return C3(0,255,255); }
} //namespace
