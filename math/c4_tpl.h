#include <iostream>
#include <utility>

using namespace std::rel_ops;


template <class T>class C4_tpl {
public:
	T x,y,z,w;

	C4_tpl() {x=0;y=0;z=0;w=0;}
	C4_tpl(T nx,T ny,T nz,T nw) {
		x=nx;y=ny;z=nz;w=nw;
	}


	void from(C3_tpl <T> c) {
		x=c.x;
		y=c.y;
		z=c.z;
	}

	void toarray(T *a) {
		a[0]=x;
		a[1]=y;
		a[2]=z;
		a[3]=w;
	}



	T distance_from_plane(C3_tpl <T> p) {
		return x*p.x+y*p.y+z*p.z+w;
	}


	friend bool operator==(const C4_tpl &a,const C4_tpl &b) {
		if (a.x!=b.x) return false;
		if (a.y!=b.y) return false;
		if (a.z!=b.z) return false;
		if (a.w!=b.w) return false;
		return true;
	}

	C4_tpl operator+(C4_tpl <T>a) {
		C4_tpl <T> c;
		c.x=a.x+x;
		c.y=a.y+y;
		c.z=a.z+z;
		c.w=a.w+w;
		return c;
	}
	C4_tpl operator-() { C4_tpl c(-x,-y,-z,-w); return c;}
	C4_tpl operator-(T b) {
		C4_tpl <T> c;

		c.x=x-b;
		c.y=y-b;
		c.z=z-b;
		c.w=w-b;
		return c;
	}

	C4_tpl operator*=(T b) {
		x*=b;
		y*=b;
		z*=b;
		w*=b;
		return (*this);
	}
	C4_tpl<T> operator/(T b) {
		C4_tpl <T> c;
		c.x=x/b;
		c.y=y/b;
		c.z=z/b;
		c.w=w/b;
		return c;
	}
};

template <class T> std::ostream& operator<< (std::ostream& s, C4_tpl <T>& v) {
   return s << v.x << " " << v.y << " " << v.z << " " << v.w;
}
template <class T> std::istream& operator>> (std::istream& s, C4_tpl <T>& v) {
	s >> v.x;
	s >> v.y;
	s >> v.z;
	s >> v.w;
	return s;
}
