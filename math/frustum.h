#ifndef __frustum_h
#define __frustum_h

#include "SDL_opengl.h"
#include <string>

class Frustum {
	bool changed_;
	C4_tpl <float> planes[6];
	unsigned int drawn_;
	unsigned int notdrawn_;
public:

	Frustum();
	void calculate();
	void calculate_if();
	//bool test(Box3_tpl <float> b);
	bool test_point(C3_tpl <float> p,float radius=15.0f);
	bool test_corners(C3f pos,float s,float radius);
	std::string tostring();

	void changed();

	
	void stat_reset();
	void stat_print();
	void drawn();
	void notdrawn();


	static Frustum * instance();
};

#endif
