#ifndef __el__game_h
#define __el__game_h

#include <iostream>
#include "SDL.h"

#include "container.h"
#include "menu.h"

#include "coordinate.h"
#include "release.h"

#include "elements/energy.h"
#include "elements/timeleft.h"
#include "elements/interval.h"
#include "elements/score.h"
#include "elements/difficulty.h"

#include "block/map2.h"

namespace PRJID {

class Entity;


typedef enum E_Action {
	A_NONE,
	A_START,
	A_QUIT,
	
	A_FORWARD,
	A_BACKWARD,
	A_LEFT,
	A_RIGHT,
	A_JUMP,
	A_TURNLEFT,
	A_TURNRIGHT,
	A_TURN,
	A_FIRE,
	A_ADDENEMY,
	A_ADDENEMY2,
	A_DAMAGE,
	A_DESTROY,

	A_LAST
};


typedef enum E_GameState {
	GS_NONE,
	GS_PLAYING,
	GS_FINISHED,
	GS_NEXTLEVEL,
	GS_QUIT
};



class Game {
	Container container_;
	Entity * player_;
	block::Map2 map2;
	unsigned int ticks_;


	void draw_hud();
	//void draw_skybox();
	void rotrans();
	//void fog();


	unsigned int dominating_direction;

	Menu menu;
	bool menu_active;

	unsigned int level;

	E_GameState state;
	Difficulty difficulty;
	Score score;

	Interval regen_interval;
	Energy destruction;
	Timeleft nextwave;
	Energy weapon;
	Energy jumpjets;
public:
	Game(Game *g=NULL,E_Difficulty d=D_EASY);
	~Game();


	Entity *get_player() { return player_; }
	unsigned int get_score() { return score.get(); }
	unsigned int get_level() { return level; }
	E_Difficulty get_difficulty() { return difficulty.get(); }
	E_GameState get_state() { return state; }

	bool isactive();

	int action(E_Action id,Entity *who=NULL,int x=0,int y=0);

	void cap();

	bool isvalid(C3f p);
	bool invalidahead(C3f p,C3 *ret);

	void destroy(C3f p);

	void incr_score();
	void tick(unsigned int ticks);

	void draw();
	void handle_state();
	bool handle_event(SDL_Event event);
	bool handle_game_event(SDL_Event event);

};

std::ostream& operator<<(std::ostream&s, Game);

} //namespace

#endif
