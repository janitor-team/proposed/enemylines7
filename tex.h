#ifndef __el__tex_h
#define __el__tex_h

#include <iostream>

#include "release.h"
#include "SDL_opengl.h"

namespace PRJID {


class Tex {
	static GLuint loadimg(const char *name,std::string dir="");
public:
	static GLuint load(std::string filename,std::string path);
};

} //namespace

#endif
