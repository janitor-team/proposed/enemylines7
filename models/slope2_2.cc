#include "SDL_opengl.h"
#include "slope2_2.h"


#include <iostream>

namespace models {

static GLuint slope2_2_dl=0;

void slope2_2::draw() {
	dldraw();
}
void slope2_2::dldraw() {
	if (slope2_2_dl==0) { sdraw(); return; }
	glCallList(slope2_2_dl);
}

void slope2_2::gen_dl() {
	slope2_2_dl=glGenLists(1);
	glNewList(slope2_2_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded slope2_2 " << slope2_2_dl << std::endl;
}
float slope2_2::minx() { return -6.1149e-17 ; }
float slope2_2::miny() { return 0 ; }
float slope2_2::minz() { return 0 ; }
float slope2_2::maxx() { return 1; }
float slope2_2::maxy() { return 1; }
float slope2_2::maxz() { return 1; }
float slope2_2::radius() { return 1.73205; }
void slope2_2::sdraw() {
const float vertices[13][3]={
	{1.00000000,0.0000000e+0,1.00000000},
	{1.00000000,1.00000000,1.00000000},
	{0.62665736,0.63600255,0.61237137},
	{1.00000000,0.0000000e+0,1.1102230e-16},
	{-6.1149003e-17,0.0000000e+0,1.00000000},
	{-6.1149003e-17,1.00000000,1.00000000},
	{0.0000000e+0,1.00000000,0.0000000e+0},
	{0.0000000e+0,0.0000000e+0,0.0000000e+0},
	{0.81332868,0.31800127,0.30618568},
	{0.31332868,0.81800127,0.30618568},
	{0.76292259,0.76980688,0.37836044},
	{0.80212357,0.70302661,0.31406144},
	{0.69712357,0.80802661,0.31406144},
};
const float normals[13][3]={
	{0.68094837,-0.51783652,0.51783652},
	{0.69031639,0.69438129,0.20321887},
	{0.69933904,0.70407918,0.12327780},
	{0.70026420,-0.19341043,-0.68718444},
	{-0.57735027,-0.57735027,0.57735027},
	{-0.51849270,0.67994900,0.51849270},
	{-0.19576185,0.70194464,-0.68479999},
	{-0.51579499,-0.51579499,-0.68404024},
	{0.75486187,0.24967964,-0.60650115},
	{0.24677685,0.75587460,-0.60642788},
	{0.67032419,0.67334847,0.31187711},
	{0.82764205,0.37837938,-0.41453308},
	{0.37587668,0.82739953,-0.41728497},
};
//o cube1
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
glEnd();
}
} //namespace
