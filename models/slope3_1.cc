#include "SDL_opengl.h"
#include "slope3_1.h"


#include <iostream>

namespace models {

static GLuint slope3_1_dl=0;

void slope3_1::draw() {
	dldraw();
}
void slope3_1::dldraw() {
	if (slope3_1_dl==0) { sdraw(); return; }
	glCallList(slope3_1_dl);
}

void slope3_1::gen_dl() {
	slope3_1_dl=glGenLists(1);
	glNewList(slope3_1_dl,GL_COMPILE);
	sdraw();
	glEndList();
	std::cout << " loaded slope3_1 " << slope3_1_dl << std::endl;
}
float slope3_1::minx() { return -1.73472e-16 ; }
float slope3_1::miny() { return 0 ; }
float slope3_1::minz() { return 0 ; }
float slope3_1::maxx() { return 1; }
float slope3_1::maxy() { return 1; }
float slope3_1::maxz() { return 1; }
float slope3_1::radius() { return 1.73205; }
void slope3_1::sdraw() {
const float vertices[13][3]={
	{1.00000000,0.0000000e+0,1.1102230e-16},
	{1.00000000,1.00000000,1.1102230e-16},
	{0.61237137,0.64157273,0.57245759},
	{0.0000000e+0,0.0000000e+0,0.0000000e+0},
	{1.00000000,0.0000000e+0,1.00000000},
	{1.00000000,1.00000000,1.00000000},
	{-1.7347235e-16,1.00000000,1.00000000},
	{-1.7347235e-16,0.0000000e+0,1.00000000},
	{0.30618568,0.30292687,0.42654675},
	{0.30618568,0.86092390,0.86087302},
	{0.37836044,0.75749786,0.42069074},
	{0.31406144,0.68638223,0.39004947},
	{0.31406144,0.80356161,0.48125798},
};
const float normals[13][3]={
	{-3.5709667e-2,0.19075907,-0.98098715},
	{-0.48107143,0.69675535,-0.53207355},
	{-0.18450149,0.68115968,-0.70850596},
	{-0.61664132,0.16234421,-0.77032321},
	{0.57735027,-0.57735027,0.57735027},
	{0.12818810,0.98081537,-0.14686396},
	{-0.58788442,0.80261546,-0.10099671},
	{-0.69911403,-0.52018384,0.49055922},
	{-0.43677832,0.48152788,-0.75983919},
	{-0.37049346,0.76411354,-0.52807679},
	{0.31187711,0.58527988,-0.74845182},
	{-0.41453308,0.27311956,-0.86808296},
	{-0.41728497,0.77456288,-0.47531632},
};
//o cube1
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[1]);
	glVertex3fv(vertices[1]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[0]);
	glVertex3fv(vertices[0]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[4]);
	glVertex3fv(vertices[4]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[7]);
	glVertex3fv(vertices[7]);
	glNormal3fv(normals[6]);
	glVertex3fv(vertices[6]);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[3]);
	glVertex3fv(vertices[3]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[5]);
	glVertex3fv(vertices[5]);
	glNormal3fv(normals[2]);
	glVertex3fv(vertices[2]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[9]);
	glVertex3fv(vertices[9]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[8]);
	glVertex3fv(vertices[8]);
glEnd();
glBegin(GL_POLYGON);
	glNormal3fv(normals[11]);
	glVertex3fv(vertices[11]);
	glNormal3fv(normals[12]);
	glVertex3fv(vertices[12]);
	glNormal3fv(normals[10]);
	glVertex3fv(vertices[10]);
glEnd();
}
} //namespace
