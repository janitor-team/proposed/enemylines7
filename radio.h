#ifndef __el__radio_h
#define __el__radio_h

#include <iostream>
#include <list>
#include <set>
#include "release.h"

#include "coordinate.h"

#include "SDL_opengl.h"

namespace PRJID {


typedef enum E_RadioTrigger {
	RT_NONE,
	RT_TEST,
	RT_START,
	RT_GOOD,
	RT_NEARLYDEAD,
	RT_TOOFAR,
	RT_WIN,
	RT_DEAD,
	RT_LAST
};

class RadioEntry {
public:
	RadioEntry(E_RadioTrigger t,GLuint tex,unsigned int d=0) {trigger=t; duration=d; texture=tex; seqnum=0;}
	E_RadioTrigger trigger;
	unsigned int duration;
	unsigned int seqnum;
	GLuint texture;
};

class TriggerTimer {
public:
	E_RadioTrigger trigger;
	unsigned int time;
};

class Radio {
	E_RadioTrigger current;
	unsigned int triggertime;
	unsigned int myticks;
	unsigned int seqnum;
	std::list <RadioEntry> entries;
	std::list <TriggerTimer> timers;
	std::set <E_RadioTrigger> had;
public:
	Radio();
	bool load(std::string path);
	void add(RadioEntry e);



	GLuint get();
	void trigger(E_RadioTrigger t);
	void trigger_once(E_RadioTrigger t);

	void tick(unsigned int ticks);
	void draw();


	static Radio *instance();
};

std::ostream& operator<<(std::ostream&s, Radio);

} //namespace

#endif
